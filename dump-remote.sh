#!/bin/bash

# Execute this script to dump the remote mysql database to a local backup file
# Run after dump-local.sh
# Requirements: a) SSH_USER must have root rights, b) MYSQL_USER must be root and can not have a password.

while getopts p: flag
do
    case "${flag}" in
        p) env_prefix=${OPTARG};;
    esac
done

now=$(date '+%Y_%m_%d-%H:%M:%S')
dir="$(dirname $(greadlink -f  $0 || readlink -f $0))"

DB_PASS_VAR_NAME="${env_prefix}_DB_PASS"
DB_HOST_VAR_NAME="${env_prefix}_DB_HOST"
DB_PORT_VAR_NAME="${env_prefix}_DB_PORT"
DB_USER_VAR_NAME="${env_prefix}_DB_USER"
DATABASE_VAR_NAME="${env_prefix}_DATABASE"

POSTGRES_DB_PASS="${!DB_PASS_VAR_NAME}"
POSTGRES_DB_HOST="${!DB_HOST_VAR_NAME}"
POSTGRES_DB_PORT="${!DB_PORT_VAR_NAME}"
POSTGRES_DB_USER="${!DB_USER_VAR_NAME}"
POSTGRES_DATABASE="${!DATABASE_VAR_NAME}"

ssh ${SERVER_PROD_SSH_USER}@${SERVER_PROD_IP} PGPASSWORD="${POSTGRES_DB_PASS}" pg_dump \
    --host="${POSTGRES_DB_HOST}" \
    --port="${POSTGRES_DB_PORT}" \
    --username="${POSTGRES_DB_USER}" --no-tablespaces --no-owner "${POSTGRES_DATABASE}" > ${dir}/dumps-remote/${now}.sql
