#!/bin/bash

dir="$(dirname $(greadlink -f  $0 || readlink -f $0))"
last=$(ls -1 $dir/dumps-remote | tail -n 1)
cp ${dir}/dumps-remote/${last} ${dir}/initialization/init.sql
