# Postgres Docker database

A repository for a postgres database run via docker and some helper scripts.

Meant to be cloned to a folder in your project, where the parent directory provides a docker-compose file and env variables.

## Getting started

- Clone the repository
- Make all scripts executable: `chmod u+x *.sh`
- Make sure that all needed env variables for the bash scripts are defined

## Docker Compose

Copy this into the docker-compose.yml file of the main repo:

```yml
<postgres-service-name>:
    build:
        context: ./<postgres-git-folder-name>
        dockerfile: postgres-dev.Dockerfile
        args: 
            POSTGRES_DEV_VERSION: ${<postgres-service-env-var-prefix>_VERSION}
    container_name: ${PROJECT_NAME}-<postgres-service-name>
    ports:
        - '${<postgres-service-env-var-prefix>_DB_PORT}:5432'
    volumes:
        - <postgres-service-name>-data:/var/lib/postgresql/data
        - ./<postgres-git-folder-name>/initialization/:/docker-entrypoint-initdb.d/
        - ./<postgres-git-folder-name>/dumps-local:/mnt/dumps-docker-host
    environment:
        POSTGRES_DB: ${<postgres-service-env-var-prefix>_DATABASE}
        POSTGRES_USER: ${<postgres-service-env-var-prefix>_DB_USER}
        POSTGRES_PASSWORD: ${<postgres-service-env-var-prefix>_DB_PASS}
        POSTGRES_DEV_DB_HOST: ${<postgres-service-env-var-prefix>_DB_HOST}
        POSTGRES_DEV_DB_PORT: ${<postgres-service-env-var-prefix>_DB_PORT}
        POSTGRES_DEV_DB_USER: ${<postgres-service-env-var-prefix>_DB_USER}
        POSTGRES_DEV_DB_PASS: ${<postgres-service-env-var-prefix>_DB_PASS}
        POSTGRES_DEV_DATABASE: ${<postgres-service-env-var-prefix>_DATABASE}
    networks: 
        - app-network
adminer:
    image: adminer
    ports:
        - ${ADMINER_DEV_PORT}:8080
    networks: 
        - app-network

# ...

networks: 
  app-network:
    name: ${PROJECT_NAME}

volumes:
  <postgres-service-name>-data:
    name: ${PROJECT_NAME}-<postgres-service-name>-data
```

### Adjustments

Then adjust the following strings to your liking:

- `<postgres-service-name>`: The docker service name, e.g. `postgres-cms`
- `<postgres-git-folder-name>`: The name of the folder in the main repo that this repo was cloned into, e.g. `postgres-cms`
- `<postgres-service-env-var-prefix>`: The environment variable prefix for this postgres service, e.g. `POSTGRES_CMS_DEV`

### Env Variables

Make sure that all env variables/secrets are defined inside the main repos config-dev.sh and config-prod.sh. Fill in the values to the following vars:

**This documentation ist still incomplete!**
