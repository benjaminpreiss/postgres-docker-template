# syntax=docker/dockerfile:1
ARG POSTGRES_DEV_VERSION
FROM postgres:$POSTGRES_DEV_VERSION

COPY dump-local.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/dump-local.sh
ENTRYPOINT ["/usr/local/bin/dump-local.sh"]

CMD ["postgres"]