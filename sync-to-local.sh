#!/bin/bash

while getopts v:p:c: flag
do
    case "${flag}" in
        v) volume_name=${OPTARG};;
        p) env_prefix=${OPTARG};;
        c) container_name=${OPTARG};;
    esac
done

dir="$(dirname $(greadlink -f  $0 || readlink -f $0))"
source ${dir}/../source-env.sh

${dir}/dump-remote.sh -p $env_prefix
${dir}/copy-dump.sh
${dir}/clear-data.sh -v $volume_name -c $container_name
