#!/bin/bash

# Run after executing script "dump-remote.sh".

while getopts v:p:c: flag
do
    case "${flag}" in
        p) env_prefix=${OPTARG};;
    esac
done

dir="$(dirname $(greadlink -f  $0 || readlink -f $0))"
last=$(ls -1 ${dir}/dumps-local | tail -n 1)
source ${dir}/../source-env.sh

DB_PASS_VAR_NAME="${env_prefix}_DB_PASS"
DB_HOST_VAR_NAME="${env_prefix}_DB_HOST"
DB_PORT_VAR_NAME="${env_prefix}_DB_PORT"
DB_USER_VAR_NAME="${env_prefix}_DB_USER"
DATABASE_VAR_NAME="${env_prefix}_DATABASE"

POSTGRES_DB_PASS="${!DB_PASS_VAR_NAME}"
POSTGRES_DB_HOST="${!DB_HOST_VAR_NAME}"
POSTGRES_DB_PORT="${!DB_PORT_VAR_NAME}"
POSTGRES_DB_USER="${!DB_USER_VAR_NAME}"
POSTGRES_DATABASE="${!DATABASE_VAR_NAME}"

ssh ${SERVER_PROD_SSH_USER}@${SERVER_PROD_IP} PGPASSWORD=${POSTGRES_DB_PASS} psql --username="${POSTGRES_DB_USER}" --host="${POSTGRES_DB_HOST}" --port="${POSTGRES_DB_PORT}" ${POSTGRES_DATABASE} < ${dir}/dumps-local/${last}
