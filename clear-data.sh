#!/bin/bash

# Execute this script to remove local mysql data.

while getopts v:c: flag
do
    case "${flag}" in
        v) volume_name=${OPTARG};;
        c) container_name=${OPTARG};;
    esac
done

dir="$(dirname $(greadlink -f  $0 || readlink -f $0))"
source ${dir}/../source-env.sh

docker-compose --env-file ./config-dev.env rm ${container_name}
docker volume rm ${volume_name}