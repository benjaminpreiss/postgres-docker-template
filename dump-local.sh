#!/bin/sh

local_dump() {
    now=$(date '+%Y_%m_%d-%H:%M:%S')
    PGPASSWORD="${POSTGRES_PASSWORD}"
    pg_dump \
        --host="${POSTGRES_DB_HOST}" \
        --port="${POSTGRES_DB_PORT}" \
        --username="${POSTGRES_USER}" --no-tablespaces --no-owner "${POSTGRES_DB}" > /mnt/dumps-docker-host/${now}.sql
}

trap 'local_dump' HUP INT QUIT TERM

exec /usr/local/bin/docker-entrypoint.sh "$@" &
wait $!
